Lista de comandos
=========================

Convenções usadas: Sinais de maior, menor ("<" e ">") são usados para indicar um espaço reservado para algum valor, e não fazem parte do comando. Colchetes ("[" e "]") são usados para indicar que um valor é opcional. Especificações do protocolo em [RFC 2812]( https://tools.ietf.org/html/rfc2812)

#### 1.1   ADMIN
##### Sintaxe

#### 1.2   AWAY
##### Sintaxe

#### 1.3   CNOTICE
##### Sintaxe

#### 1.4   CPRIVMSG
##### Sintaxe

#### 1.5   CONNECT
##### Sintaxe

#### 1.6   DIE
##### Sintaxe

#### 1.7   ENCAP
##### Sintaxe

#### 1.8   ERROR
##### Sintaxe

#### 1.9   HELP
##### Sintaxe

#### 1.10  INFO
##### Sintaxe

#### 1.11  INVITE
##### Sintaxe

#### 1.12  ISON
##### Sintaxe

#### 1.13  JOIN
##### Sintaxe

#### 1.14  KICK
##### Sintaxe

#### 1.15  KILL
##### Sintaxe

#### 1.16  KNOCK
##### Sintaxe

#### 1.17  LINKS
##### Sintaxe

#### 1.18  LIST
##### Sintaxe

#### 1.19  LUSERS
##### Sintaxe

#### 1.20  MODE
##### Sintaxe

```
MODE <nickname> <( "+" / "-" )flag>
```
<p>O modo de usuário são tipicamente alterações que afetam como o cliente é visto pelos outros clientes ou o tipo de mensagens 'extra' o cliente envia</p>
<p>Um comando MODE <b>deve</b> ser aceito somente se o remetente da mensagem e o apelido dado como 
 parâmetro for o mesmo. E se nenhum outro parâmetro é dado, então o servidor retornará a atual configurações para o nick.</p> 

Os modos disponíveis são:

* a - user is flagged as away;
* i - marks a users as invisible;
* w - user receives wallops;
* r - restricted user connection;
* o - operator flag;
* O - local operator flag;
* s - marks a user for receipt of server notices.

<p>A flag 'a' <b>não</b> deve ser alterada pelo usuário usando o comando MODE, em vez disso, o uso do comando AWAY é <b>necessário</b>.</p>

<p>Se um usuário tentar se tornar um operador usando as flags "+ o" ou "+ O", a tentativa <b>deve</b> ser ignorada como os usuários podem ignorar o mecanismos de autenticação do comando OPER.</p>

<p>Por outro lado, se um usuário tentar se tornar irrestrito usando o sinalizador "-r", a tentativa deve ser ignorada. Este sinalizador é tipicamente configurado pelo servidor na conexão para razões administrativas. Enquanto as restrições impostas são deixadas para a implementação, é típico que um usuário restrito não tenha permissão para alterar apelidos, nem fazer uso do operador do canal.</p>

Respostas:

* ERR_NEEDMOREPARAMS
* ERR_USERSDONTMATCH
* ERR_UMODEUNKNOWNFLAG
* RPL_UMODEIS

Exemplos:

```
MODE WiZ -w 
```
Comando para Wiz não receber mensagens WALLOPS.
```
MODE Angel +i
```
Comando para fazer com que Angel fique invisível.

Definido em [RFC 2812](https://tools.ietf.org/html/rfc2812#section-3.1.2)

#### 1.21  MOTD
##### Sintaxe

#### 1.22  NAMES
##### Sintaxe

#### 1.23  NAMESX
##### Sintaxe

#### 1.24  NICK
##### Sintaxe
```
NICK <nickname>
```
<p>Permite que um cliente altere seu apelido irc.</p>
Respostas:
* ERR_NONICKNAMEGIVEN             
* ERR_ERRONEUSNICKNAME
* ERR_NICKNAMEINUSE               
* ERR_NICKCOLLISION
* ERR_UNAVAILRESOURCE             
* ERR_RESTRICTED

Exemplos:
```
NICK Wiz
```
<p>Criado novo nick "Wiz" se a sessão continua não registrada, ou o usuário alterou seu nickname para "Wiz"</p>

Definido em [RFC 2812](https://tools.ietf.org/html/rfc2812#section-3.1.2)

#### 1.25  NOTICE
##### Sintaxe

#### 1.26  OPER
##### Sintaxe
```
OPER <nome> <senha>
```

Um usuario normal pode usar o comando OPER para obter privilégios. A combinacao < nome > e < senha > é <b>obrigatória</b> para obter os privilégios de operador. Após sucesso, o usuário irá receber uma mensagem MODE indicando o novo modo de usuário. (veja [sessão 1.20](https://gitlab.com/ad-si-2018-1/p1-g4/wikis/comandos#1.20))

   Numeric Replies:

           ERR_NEEDMOREPARAMS              RPL_YOUREOPER
           ERR_NOOPERHOST                  ERR_PASSWDMISMATCH

   Example:

   OPER foo bar                    ; Attempt to register as an operator
                                   using a username of "foo" and "bar"
                                   as the password.

#### 1.27  PART
##### Sintaxe

#### 1.28  PASS
##### Sintaxe
```
PASS <password>
```
<p>O comando PASS é usado para definir uma 'senha de conexão'. A senha opcional deve ser definida antes do registro da conexão ser realizado. Requer que o comando PASS seja executado antes da combinação de comandos NICK/USER.</p>

Respostas numéricas:
* ERR_NEEDMOREPARAMS              
* ERR_ALREADYREGISTRED

Exemplo:
```
PASS senhasecretaaqui
```
Definido por [RFC 2812](https://tools.ietf.org/html/rfc2812#section-3.1.1)
#### 1.29  PING
##### Sintaxe

#### 1.30  PONG
##### Sintaxe

#### 1.31  PRIVMSG
##### Sintaxe

#### 1.32  QUIT
##### Sintaxe
```
QUIT [<mensagem de saída>]
```

A client session is terminated with a quit message.  The server acknowledges this by sending an ERROR message to the client.

#### 1.33  REHASH
##### Sintaxe

#### 1.34  RESTART
##### Sintaxe

#### 1.35  RULES
##### Sintaxe

#### 1.36  SERVER
##### Sintaxe

#### 1.37  SERVICE
##### Sintaxe

#### 1.38  SERVLIST
##### Sintaxe

#### 1.39  SQUERY
##### Sintaxe

#### 1.40  SQUIT
##### Sintaxe

#### 1.41  SETNAME
##### Sintaxe

#### 1.42  SILENCE
##### Sintaxe

#### 1.43  STATS
##### Sintaxe

#### 1.44  SUMMON
##### Sintaxe

#### 1.45  TIME
##### Sintaxe

#### 1.46  TOPIC
##### Sintaxe

#### 1.47  TRACE
##### Sintaxe

#### 1.48  UHNAMES
##### Sintaxe

#### 1.49  USER
##### Sintaxe
```
USER <usuário> <modo> <nome_real>
```

<p>O comando USER no início de uma conexão para especificar o nome de usuário, o nome do host e o nome real de um novo usuário.</p>
<p>
O parâmetro <modo> deve ser um valor numérico, e pode ser usado para enviar automaticamente o modo do usuário quando se registrar no servidor. Este parâmetro é uma mascara de bit, tendo apenas 2 bits com significancia: se o bit 2 é definido, o modo de usuário será 'w' e se o bit 3 é definido, o modo de usuário será 'i'.
</p>
<p>O parâmetro <nome_real> pode conter caracteres de espaço.</p>

Respostas numéricas:
* ERR_NEEDMOREPARAMS              
* ERR_ALREADYREGISTRED

Examplos:
```
USER teste 8 * :Joao Silva 
```                                   
Usuário se registra com nome de usuário "teste" e nome real "Joao Silva", pedindo para servidor acessar em modo invisível
Definido por [RFC 2812](https://tools.ietf.org/html/rfc2812#section-3.1.3)

#### 1.50  USERHOST
##### Sintaxe

#### 1.51  USERIP
##### Sintaxe

#### 1.52  USERS
##### Sintaxe

#### 1.53  VERSION
##### Sintaxe

#### 1.54  WALLOPS
##### Sintaxe

#### 1.55  WATCH
##### Sintaxe

#### 1.56  WHO
##### Sintaxe

#### 1.57  WHOIS
##### Sintaxe

#### 1.58  WHOWAS
##### Sintaxe