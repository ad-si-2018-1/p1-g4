### Teste de cadastro de usuário

### Executar o comando, utilizando o padrão -> USER NICK O * nomeUSUARIO
### Pré condição - ter um nick registrado no server

```
user gleydson o * gleydson
```

## Saída obtida

:localhost 001 gleydson :Bem vindo ao servidor IRC da disciplina Aplicações Distribuídas gleydson

:localhost 002 gleydson :Seu host é ::ffff:127.0.0.1, rodando versão irc-ad 1.0.0

:localhost 003 gleydson :Este servidor foi criado em April 22, 2018 8:30 PM

:localhost 004 gleydson :localhost 1.0.0 wiao s

:localhost 251 gleydson :Existem 1 usuarios em 1 servidor

:localhost 252 gleydson :Existe(m) 0 administrador(es) online

:localhost 253 gleydson :Existe(m) 0 conexo(es) desconhecida(s)

:localhost 254 gleydson 2 :Canal(is) formado(s)

:localhost 255 gleydson :Tenho 1 clientes e 1 servidor

:localhost 375 gleydson :localhost Mensagem do dia

:localhost 372 gleydson :Servidor IRC rodando \o/\o/\o/\o/\o/\o/

:localhost 376 gleydson :Fim do comando MOTD


### Resultado do teste = Sucesso