### Teste do comando TIME

### Executar o comando, utilizando o padrão -> TIME endereçoDoServidor
### Pré condição - ter um nick registrado no server, ter um user cadastrado no server

```
time localhost
```

## Saída obtida 

localhost :Domingo, 22 de abril de 2018

### Resultado do teste = Sucesso