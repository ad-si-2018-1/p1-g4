/**
 *******************************************************************************
 * Autor: Grupo 4 de Aplicações Distribuídas (2018-1)          Data: 08/04/2018 *
 *                                                                              *
 * Projeto 1 - Servidor IRC em nodejs                                           *
 *******************************************************************************
 */

// Carregando o módulo NET que fornece uma API de rede assíncrona para criar servidores TCP
var net = require('net');
var moment = require('moment');
const command_reply = require('./respostas');
const porta = process.env.PORTA || 6667;
// Models
var Client = require('./model/client');
var Chanel = require('./model/chanel');

module.exports = {
  list_clients: [],
  list_chanels: [new Chanel('public'), new Chanel('secret', undefined, ['s'], 'secret')],
  server_name: 'irc.ad-g4',
  server_pass: 'servidor',
  version: '1.0.0',
  administrator: 'Grupo 4 da disciplina de Aplicações Distribuídas de 2018/1 - INF/UFG',

  /**
   * w - usuario pode receber WALLOPS
   * i - usuário invisível
   * a - usuário ausente
   * o - usuário operador
   */
  user_modes: ['w', 'i', 'a', 'o'],
  /**
   * s - Chat secreto
   */
  chanel_modes: ['s'],
  start: moment().format('LLL'),
  message_of_day: 'Servidor IRC rodando \\o/\\o/\\o/\\o/\\o/\\o/',

  sendCommandToClient: function(comando, client, parametros, mensagem, prefixo) {
    var message = mensagem.split('\n');
    var pre =  ':' + ((prefixo) ? prefixo : module.exports.server_name) + ' ' +
      ((typeof comando == 'string') ? comando.toUpperCase(): comando) + ' ' +
      ((parametros) ? parametros.join(' '): '') + ' ';

    if (message) {
      message.forEach(function(m) {
        client.socket.write(pre + ':' + m +'\n');
      });
    } else {
      client.socket.write(pre + '\n');
    }
  },

  readCommandFromClient: function(comandos, client){
    const list_commands = require('./comandos/listaComandos');
    var mensagem = String(comandos).trim();
    // o método split quebra a mensagem em partes separadas p/ espaço em branco
    var linhas = mensagem.split(/(\r\n|\n|\r)/);

    linhas.forEach(function(comando){
      comando = comando.replace(/(\r\n|\n|\r)/g, '')
      let args = comando.split(' ');
      comando = args.shift().toUpperCase();

      if (comando) {
        try {
          list_commands[comando](args, client)
        } catch (err){
          console.log(comando + ' - ' + client.nick + ' - ' + client.socket.name);
          console.log(err);
          if (!list_commands[comando]) {
            module.exports.sendCommandToClient(command_reply.ERR_UNKNOWNCOMMAND, client, [client.nick, client.name], 'Comando desconhecido');
          }
        }
      }
    });
  },

  broadcast: function(message, sender) {
    if (sender.user) {
      //
    }
    module.exports.list_clients.forEach(function (cliente) {
      // Não envia a mensagem ao remetente
      if (cliente === sender) return;
      module.exports.sendCommandToClient('notice', cliente, ['*'], ':' + message);
    });
  }
}

// Iniciando um servidor TCP
net.createServer(function (socket) {
  var client = new Client(socket);
  // Identificando o cliente através do seu endereço IP e porta utilizada pela aplicação
  socket.name = socket.remoteAddress + ':' + socket.remotePort;
  socket.setTimeout(30000, function() {
    client.close_conection = true;
    module.exports.sendCommandToClient('ping', client, [client.nick], '*** Bem vindo ao servidor IRC');

    setTimeout(function() {
      if (client.close_conection) {
        module.exports.sendCommandToClient('quit', client, [], 'Ping timeout: 40 segundos', client.nick + '!' + client.user_name + '@' + client.socket.name);
        client.socket.emit('end');
      }
    }, 10000);
  });
  // Adiciona o novo cliente na lista de clientes
  if (module.exports.list_clients.indexOf(client) < 0) {
    module.exports.list_clients.push(client);
  }
  // Envia uma mensagem de boas vindas ao novo cliente
  socket.write('Bem-vindo ' + socket.name + '\n');
  module.exports.sendCommandToClient('notice', client, [client.nick], '*** Bem vindo ao servidor IRC');
  // Anuncia a entrada do novo cliente a todos os usuários
  module.exports.broadcast(socket.name + ' entrou no chat', client);

  // Lida com o evento 'data', que vai receber as mensagens enviadas pelos clientes
  socket.on('data', function (data) {
    try {
      module.exports.readCommandFromClient(data, client, module.exports.list_clients);
    } catch (error) {
      console.log(error);
      client.socket.emit('end');
    }
  });

  socket.on('pong', function () {
    client.close_conection = false;
  });

  // Desconexão do cliente
  socket.on('end', function () {
    // Remove o cliente da lista
    module.exports.list_clients.splice(module.exports.list_clients.indexOf(client.socket), 1);
    // Anuncia a saída do cliente a todos os usuários
    module.exports.broadcast(socket.name + ' deixou o chat.\n', client);
    module.exports.sendCommandToClient('ERROR', client, [], 'Fechando conexão :' + client.socket.remoteAddress);
  });

  socket.on('error', function() {
    socket.destroy();
  });
})
  .listen(porta, function(){
    // Coloque uma mensagem amigável no terminal do servidor.
    console.log('Servidor de bate-papo executando na porta ' + porta + '\n');
  })
  .on('error', function(err){
    console.log('Ops! Houve um erro no servidor IRC. \nDetalhes do erro: ' + err);
  });
