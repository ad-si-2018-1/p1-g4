/*
 * 22/04/2018
 * Comando: ADMIN - é usado para obter informações do administrador do servidor atual ou de um determinado servidor.
 * Autor(a): Raquel Andrade
 */

const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.user_name], 'Não registrado');
    return;
  }

  /*
  * Caso não seja passado nenhum parâmetro ou seja passado o nome do nosso 
  * servidor (localhost), as informações de administrador do mesmo
  * será retornada. Caso seja passado o nome de outro servidor, por enquanto
  * retornaremos a msg de que não foi encontrado, pois nosso projeto tem um 
  * único servidor.
  */
  if (args.length == 0 || args[0] == server.server_name) {
    server.sendCommandToClient(command_reply.RPL_ADMINME, client, [server.server_name], 
	'Administrador do servidor ' + server.server_name + ': ' + server.administrator);
    return;
  } else if (args[0] != server.server_name) {
    server.sendCommandToClient(command_reply.ERR_NOSUCHSERVER, client, [client.nick, args[0]], 'Servidor não encontrado');
    return;
  }
}
