/*
 * 15/04/2018
 * Comando: TIME - é usado para consultar a hora local do servidor especificado.
 * Autor(a): Raquel Andrade
 */

const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.user_name], 'Não registrado');
    return;
  }

  if (args[0] != server.server_name) {
    server.sendCommandToClient(command_reply.ERR_NOSUCHSERVER, client, [client.nick, args[0]], 'Servidor não encontrado');
    return;
  }

  var diasSemana = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
  var meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

  var dataAtual = new Date();
  var dia = diasSemana[dataAtual.getDay()];
  var mes = meses[dataAtual.getMonth()];

  server.sendCommandToClient(command_reply.RPL_TIME, client, [server.server_name], dia + ', ' + dataAtual.getDate() + ' de ' + mes + ' de ' + dataAtual.getFullYear() + '.');
}
