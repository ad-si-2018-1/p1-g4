/*
 * 22/04/2018
 * Comando: VERSION - é usado para consultar a versão de um servidor.
 * Autor(a): Raquel Andrade
 */

const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.user_name], 'Não registrado');
    return;
  }

  /*
  * O único parâmetro esperado para esse comando é o nome de um servidor,
  * porém esse comando é opcional, sendo assim, caso não seja passado nada
  * ou seja passado o nome do nosso servidor (localhost), a versão do mesmo
  * será retornada. Caso seja passado o nome de outro servidor, por enquanto
  * retornaremos a msg de que não foi encontrado, pois nosso projeto tem um 
  * único servidor.
  */
  if (args.length == 0 || args[0] == server.server_name) {
    server.sendCommandToClient(command_reply.RPL_VERSION, client, [server.server_name], 'Versão ' + server.version);
    return;
  } else if (args[0] != server.server_name) {
    server.sendCommandToClient(command_reply.ERR_NOSUCHSERVER, client, [client.nick, args[0]], 'Servidor não encontrado');
    return;
  }
}
