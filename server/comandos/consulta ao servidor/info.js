/*
 * 22/04/2018
 * Comando: INFO - é usado para retornar informações descrevendo o
    servidor: sua versão, quando foi compilado, o patchlevel, quando
    foi iniciado, e qualquer outra informação diversa que possa ser
    considerado relevante.
 * Autor(a): Raquel Andrade
 */

const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.user_name], 'Não registrado');
    return;
  }

  if (args.length == 0 || args[0] == server.server_name) {
    server.sendCommandToClient(command_reply.RPL_INFO, client, [server.server_name], 
	'Informações sobre o servidor ' + server.server_name + ': versão ' + server.version 
	+ ', administrado por ' + server.administrator + '.');
    return;
  } else if (args[0] != server.server_name) {
    server.sendCommandToClient(command_reply.ERR_NOSUCHSERVER, client, [client.nick, args[0]], 'Servidor não encontrado');
    return;
  }
}
