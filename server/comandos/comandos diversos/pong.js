const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client, list_clientes) {

  if (args == null || args.length < 1) {
    server.sendCommandToClient(command_reply.ERR_NOORIGIN, client, [client.nick], ':Origem não especificada');
  }
  client.socket.emit('pong');
}
