const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  var close_conection = true;

  if (args == null || args.length < 1) {
    server.sendCommandToClient(command_reply.ERR_NOORIGIN, client, [client.nick], 'Origem não especificada');
  } else if (args.length == 1) {
    server.sendCommandToClient('pong', client, [client.nick], args[0]);
  } else {
    if (args[1] == server.server_name) {
      server.sendCommandToClient('pong', client, [client.nick], server.server_name);
    }
  }
}
