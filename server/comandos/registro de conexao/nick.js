/*
 * 08/04/2018
 * Comando: NICK - É usado para dar ao usuário um apelido ou alterar o nome existente.
 * Autor(a): Raquel Andrade
 */
const servidor = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  var flagAlteracaoNick = false;

  if (!args.length > 0) {
    servidor.sendCommandToClient(command_reply.ERR_NONICKNAMEGIVEN, client, [client.nick], 'Nick não fornecido');
    return;
  }

  let user = servidor.list_clients.find(function(e) {
    return e.nick == args[0]
  });

  if (user != null) {
    servidor.sendCommandToClient(command_reply.ERR_NICKNAMEINUSE, client, [user.nick], 'Nickname em uso');
    return;
  }

  if (client.nick != '*') {
    // se o nickname já foi atribuido ao cliente, remove nickname atual do mapa
    client.nick = undefined;
    flagAlteracaoNick = true;
  }

  var nick_old = client.nick;
  client.nick = args.shift();

  if (flagAlteracaoNick) {
    servidor.sendCommandToClient('nick', client, [], client.nick, nick_old + '!' + nick_old + '@' + client.socket.remoteAddress);
  } else {
    if (client.user_name != '') {
      servidor.sendCommandToClient(command_reply.RPL_WELCOME, client, [client.nick], 'Bem vindo ao servidor IRC da disciplina Aplicações Distribuídas ' + client.nick);
      servidor.sendCommandToClient(command_reply.RPL_YOURHOST, client, [client.nick], 'Seu host é ' + client.socket.remoteAddress + ', rodando versão irc-ad 1.0.0');
      servidor.sendCommandToClient(command_reply.RPL_CREATED, client, [client.nick], 'Este servidor foi criado em ' + servidor.start);
      servidor.sendCommandToClient(
        command_reply.RPL_MYINFO,
        client,
        [client.nick],
        servidor.server_name + ' ' + servidor.version + ' ' + servidor.user_modes.join('') + ' ' + servidor.chanel_modes.join('')
      );
      servidor.sendCommandToClient(command_reply.RPL_LUSERCLIENT, client, [client.nick], 'Existem ' + servidor.list_clients.length + ' usuarios em 1 servidor');
      servidor.sendCommandToClient(
        command_reply.RPL_LUSEROP,
        client,
        [client.nick],
        'Existe(m) ' + servidor.list_clients.filter((e) => e.isOperator).length + ' administrador(es) online'
      );
      servidor.sendCommandToClient(
        command_reply.RPL_LUSERUNKNOWN,
        client,
        [client.nick],
        'Existe(m) ' + servidor.list_clients.filter((e) => e.user == '').length + ' conexo(es) desconhecida(s)'
      );
      servidor.sendCommandToClient(command_reply.RPL_LUSERCHANNELS, client, [client.nick, servidor.list_chanels.length], 'Canal(is) formado(s)');
      servidor.sendCommandToClient(command_reply.RPL_LUSERME, client, [client.nick], 'Tenho ' + servidor.list_clients.length + ' clientes e 1 servidor');
      servidor.sendCommandToClient(command_reply.RPL_MOTDSTART, client, [client.nick], servidor.server_name + ' Mensagem do dia');
      servidor.sendCommandToClient(command_reply.RPL_MOTD, client, [client.nick], servidor.message_of_day);
      servidor.sendCommandToClient(command_reply.RPL_ENDOFMOTD, client, [client.nick], 'Fim do comando MOTD');
    }
  }
}
