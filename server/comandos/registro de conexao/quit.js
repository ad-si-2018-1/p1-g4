const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  var reply = require('../listaComandos').sendCommandToClient;
  var mensagem = (args) ? args.join(' ') : '';

  server.sendCommandToClient('quit', client, [], mensagem);
  client.socket.emit('end');
}
