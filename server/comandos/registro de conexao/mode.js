const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  if (args == null || args.length < 2) {
    server.sendCommandToClient(command_reply.ERR_NEEDMOREPARAMS, client, ['MODE'], 'Parâmetros insuficientes');
    return;
  }

  
}
