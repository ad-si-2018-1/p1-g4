const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  if (args == null || args.length < 2) {
    server.sendCommandToClient(command_reply.ERR_NEEDMOREPARAMS, client, ['oper'], 'Parâmetros insuficientes');
    return;
  }
  var user = args.shift();
  var pass = args.shift();

  if (pass == server.server_pass) {
    if (client.modes.indexOf('o') < 0) {
      client.modes.push('o');
    }
    server.sendCommandToClient(command_reply.RPL_YOUREOPER, client, [], 'Você agora é um operador');
  } else {
    server.sendCommandToClient(command_reply.ERR_PASSWDMISMATCH, client, [], 'Senha incorreta');
  }
}
