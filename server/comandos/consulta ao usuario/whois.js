const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  if (args == null, args.length == 0) {
    server.sendCommandToClient(command_reply.ERR_NONICKNAMEGIVEN, client, [client.nick], 'Nick não informado');
  } else {
    var nick = args.shift();
    var operador = args.shift() == 'o';

    server.list_chanels.filter((e) => e.modes.indexOf('s') < 0).forEach(function(canal) {
      var membros = canal.list_members.filter((e) => (e.modes.indexOf('i') < 0) && (e.nick == nick));
      if (membros == null, membros.length == 0) {
        server.sendCommandToClient(command_reply.ERR_NOSUCHNICK, client, [client.nick], 'Nick não encontrado');
      } else {
        membros.forEach(function(e) {
          server.sendCommandToClient(command_reply.RPL_WHOISUSER, client, [e.nick, e.user_name, e.socket.remoteAddress, '*'], e.real_name);
          server.sendCommandToClient(command_reply.RPL_WHOISSERVER, client, [e.nick, server.server_name], server.server_name);
          if (client.modes.indexOf('o') > -1) {
            server.sendCommandToClient(command_reply.RPL_WHOISOPERATOR, client, [e.nick], 'É um operador IRC');
          }
          server.sendCommandToClient(command_reply.RPL_WHOISIDLE, client, [e.nick, e.time_connected], 'Segundos conectados');
        });
      }
    });
  }

  server.sendCommandToClient(command_reply.RPL_ENDOFWHO, client, [client.nick], 'Fim da lista WHOIS');
}