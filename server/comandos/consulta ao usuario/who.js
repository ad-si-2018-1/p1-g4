const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  if (args == null, args.length == 0) {
    server.list_chanels.filter((e) => e.modes.indexOf('s') < 0).forEach(function(canal) {
      var nicks = [];
      canal.list_members.filter((e) => e.modes.indexOf('i') < 0).forEach(function(e) {
        nicks.push(e.nick)
      });

      server.sendCommandToClient(command_reply.RPL_WHOREPLY, client, [client.nick], nicks.join(' '));
    });
  } else {
    var mascara = args.shift();
    var operador = args.shift() == 'o';

    server.list_chanels.filter((e) => e.modes.indexOf('s') < 0).forEach(function(canal) {
      var nicks = [];
      canal.list_members.filter((e) =>
        (e.modes.indexOf('i') < 0) &&
        ((operador) ? e.modes.indexOf('o') > -1 : true) &&
        (
          (mascara.indexOf('*') > -1) ?
            (
              (
                (mascara.charAt(0) == '*') ? (
                  e.nick.indexOf(mascara.slice(1)) > -1 || e.user_name.indexOf(mascara.slice(1)) > -1 || e.socket.remoteAddress.indexOf(mascara.slice(1)) > -1
                ) : true
              ) ||
              (
                (mascara.charAt(mascara.length -1) == '*') ? (
                  e.nick.indexOf(mascara.slice(0, -1)) > -1 || e.user_name.indexOf(mascara.slice(0, -1)) > -1 || e.socket.remoteAddress.indexOf(mascara.slice(0, -1)) > -1
                ) : true
              ) &&
              (
                (mascara.charAt(mascara.length -1) == '*' && mascara.charAt(0) == '*') ? (
                  e.nick.indexOf(mascara.slice(1, -1)) > -1 || e.user_name.indexOf(mascara.slice(1, -1)) > -1 || e.socket.remoteAddress.indexOf(mascara.slice(1, -1)) > -1
                ) : true
              )
            ) : e.nick == mascara || e.user_name == mascara || e.socket.remoteAddress == mascara
        )
      ).forEach(function(e) {
        nicks.push(e.nick)
      });

      server.sendCommandToClient(
        command_reply.RPL_WHOREPLY,
        client,
        [canal.name, client.user_name, client.socket.remoteAddress, client.nick],
        nicks.join(' ')
      );
    });
  }
  server.sendCommandToClient(command_reply.RPL_ENDOFWHO, client, [client.nick], 'Fim da lista WHO');
}