module.exports = {
  //Comandos para registrar no servidor
  'PASS': require('./registro de conexao/pass'),
  'USER': require('./registro de conexao/user'),
  'NICK': require('./registro de conexao/nick'),
  'OPER': require('./registro de conexao/oper'),
  'QUIT': require('./registro de conexao/quit'),
  //Comandos para operação do canal
  'JOIN': require('./operacoes de canal/join'),
  'KICK': require('./registro de conexao/pass'),
  'TOPIC': require('./operacoes de canal/join'),
  'LIST': require('./operacoes de canal/list'),
  'INVITE': require('./registro de conexao/pass'),
  'NAMES': require('./operacoes de canal/names'),
  'PART': require('./registro de conexao/pass'),
  //Comandos do usuário
  'WHO': require('./consulta ao usuario/who'),
  'WHOIS': require('./consulta ao usuario/whois'),
  'WHOAS': require('./consulta ao usuario/who'),
  //Comandos para envio de mensagens
  'PRIVMSG': require('./envio de mensagens/privmsg'),
  'NOTICE': require('./envio de mensagens/notice'),
  //Comandos para consulta ao servidor
  'ADMIN': require('./consulta ao servidor/admin'),
  'INFO': require('./consulta ao servidor/info'),
  'TIME': require('./consulta ao servidor/time'),
  'VERSION': require('./consulta ao servidor/version'),
  //Comandos diversos
  'PING': require('./comandos diversos/ping'),
  'PONG': require('./comandos diversos/pong')
}
