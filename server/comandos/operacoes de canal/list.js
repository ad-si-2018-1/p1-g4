const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  server.list_chanels.forEach(function(chanel) {
    if (chanel.modes.indexOf('s') < 0) {
      server.sendCommandToClient(command_reply.RPL_LIST, client, [client.nick, chanel.name, chanel.list_members.length], chanel.topic);
    }
  });
  server.sendCommandToClient(command_reply.RPL_LISTEND, client, [client.nick], 'Final do comando /LIST');
}
