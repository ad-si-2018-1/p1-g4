const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  if (args == null || args.length < 1) {
    server.sendCommandToClient(command_reply.ERR_NEEDMOREPARAMS, client, ['MODE'], 'Parâmetros insuficientes');
    return;
  }

  var canal = args.shift();

  if (args.length == 1) {
    server.sendCommandToClient(command_reply.RPL_NOTOPIC, client, [canal], 'Topico não definido');
    return;
  }

  if (client.modes.indexOf('o') < 0) {
    server.sendCommandToClient(command_reply.ERR_CHANOPRIVSNEEDED, client, [canal], 'Necessário ter permissão de operador');
  } else {
    var topico = args.split(':')[1];

    server.sendCommandToClient(command_reply.RPL_TOPIC, client, [canal], topico);
  }
}