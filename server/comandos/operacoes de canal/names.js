const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  if (args.length == 0 || args == null) {
    server.list_chanels.filter((e) => e.modes.indexOf('s') < 0).forEach(function(canal) {
      var nicks = [];
      canal.list_members.filter((e) => e.modes.indexOf('i') < 0).forEach(function(e) {
        nicks.push(e.nick)
      });

      server.sendCommandToClient(command_reply.RPL_NAMREPLY, client, [client.nick], nicks.join(' '));
    });
  } else {
    var lista_canais = args[0].split(',');
    lista_canais.forEach(function(nome_canal) {
      server.list_chanels.filter((e) => e.modes.indexOf('s') < 0).forEach(function(canal) {
        var nicks = [];
        canal.list_members.filter((e) => e.modes.indexOf('i') < 0).forEach(function(e) {
          nicks.push(e.nick)
        });
        server.sendCommandToClient(command_reply.RPL_NAMREPLY, client, [client.nick, '=', canal.name], nicks.join(' '));
      });
    });
  }
  server.sendCommandToClient(command_reply.RPL_ENDOFNAMES, client, [client.nick, '*'], 'Fim da lista de NAMES');
}
