const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  if (args == null || args.length == 0) {
    server.sendCommandToClient(command_reply.ERR_NEEDMOREPARAMS, client, ['JOIN'], 'Parâmetros insuficientes');
    return;
  }

  if (args[0] == '0'){
    server.list_chanels.forEach(function (chanel) {
      client.leaveChanel(chanel);
      server.sendCommandToClient('part', client, ['#' + chanel.name], '', client.nick + '!' + client.name + '@' + client.socket.name);
    });
    return;
  }

  args = args[0].split(',');

  server.list_chanels.forEach(function (chanel) {
    if (args.indexOf(chanel.name) > -1) {
      var lista_nick = [];

      client.addInChanel(chanel);
      chanel.list_members.forEach(function (member) {
        lista_nick.push(member.user_name);
        server.sendCommandToClient('join', member, [chanel.name], '', client.nick + '!' + client.name + '@' + client.socket.name);
      });

      server.sendCommandToClient(command_reply.RPL_NAMREPLY, client, [' = ' + chanel.name], lista_nick.join(' '));
    }
  });
}
