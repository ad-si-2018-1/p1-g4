const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    return;
  }

  var destino = args.shift();
  var mensagem = args.join(' ').split(':')[1];

  if (destino == null || destino =='') {
    return;
  }

  if (mensagem != null && mensagem != '') {
    if (destino.charAt(0) == '#') {
      let canal = server.list_chanels.filter((e) => e.name == destino);
      if (!canal){
        return;
      }

      canal.forEach(function(c) {
        c.list_members.forEach(function(e) {
          if (e.nick != client.nick) {
            server.sendCommandToClient(
              'notice',
              e,
              [destino],
              mensagem,
              client.nick + '!' + client.user_name + '@' + client.socket.remoteAddress
            );
          }
        });
      });
    } else {
      client.list_chanel_connected.forEach(function(canal) {
        let member = canal.list_members.filter((e) => e.nick == destino);
        if (member.length > 0) {
          member.forEach(function(e) {
            server.sendCommandToClient('notice', e, [destino], mensagem, client.nick + '!' + client.user_name + '@' + client.socket.remoteAddress);
          });
        }
      });
    }
  }
}
