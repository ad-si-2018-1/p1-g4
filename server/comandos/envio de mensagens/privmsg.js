const server = require('../../server'),
  command_reply = require('../../respostas');

module.exports = function (args, client) {
  if (client.nick == '' || client.user_name == '') {
    server.sendCommandToClient(command_reply.ERR_NOTREGISTERED, client, [client.nick, client.name], 'Não registrado');
    return;
  }

  var destino = args.shift();
  var mensagem = args.join(' ').split(':')[1];

  if (destino == null || destino =='') {
    server.sendCommandToClient(command_reply.ERR_NOTEXTTOSEND, client, [client.nick], 'Nenhuma mensagem para enviar');
    return;
  }

  if (mensagem != null && mensagem != '') {
    if (destino.charAt(0) == '#') {
      let canal = server.list_chanels.filter((e) => e.name == destino);
      if (!canal){
        server.sendCommandToClient(command_reply.ERR_CANNOTSENDTOCHAN, client, [destino], 'Não pode ser enviado para o canal');
        return;
      }

      canal.forEach(function(c) {
        c.list_members.forEach(function(e) {
          if (e.nick != client.nick) {
            server.sendCommandToClient(
              'privmsg',
              e,
              [destino],
              mensagem,
              client.nick + '!' + client.user_name + '@' + client.socket.remoteAddress
            );
          }
        });
      });
    } else {
      client.list_chanel_connected.forEach(function(canal) {
        let member = canal.list_members.filter((e) => e.nick == destino);
        if (member.length > 0) {
          member.forEach(function(e) {
            server.sendCommandToClient('privmsg', e, [destino], mensagem, client.nick + '!' + client.user_name + '@' + client.socket.remoteAddress);
          });
        } else {
          server.sendCommandToClient(command_reply.ERR_NOSUCHNICK, client, [destino], 'Nick não encontrado');
        }
      });
    }
  } else {
    server.sendCommandToClient(command_reply.ERR_NOTEXTTOSEND, client, [client.nick], 'Nenhuma mensagem para enviar');
  }
}
