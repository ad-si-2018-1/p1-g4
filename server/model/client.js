module.exports = class Client {
  constructor(socket) {
    this._socket = socket;
    this._nick = undefined;
    this._user_name = undefined;
    this._real_name = undefined;
    this._password = undefined;
    this._flag_to_close = false;
    this._host = undefined;
    this._modes = [];
    this._list_chanel_connected = [];
    this._start_connection = Date.now();
  }

  get socket() {
    return this._socket;
  }

  set nick(value) {
    this._nick = value;
  }

  get nick() {
    return (this._nick) ? this._nick : '*';
  }

  set user_name(value) {
    this._user_name = value;
  }

  get user_name() {
    return (this._user_name) ? this._user_name : '';
  }

  set real_name(value) {
    this._real_name = value;
  }

  get real_name() {
    return (this._real_name) ? this._real_name : '';
  }

  set password(value) {
    this._password = value;
  }

  get password() {
    return (this._password) ? this._password : '';
  }

  get close_conection() {
    return this._flag_to_close;
  }

  set close_conection(value) {
    this._flag_to_close = value;
  }

  get host() {
    return this._host;
  }

  set host(value) {
    this._host = (value != null && value != '') ? value : this._host;
  }

  addInChanel(chanel) {
    if (this._list_chanel_connected.indexOf(chanel) > -1) {
      return;
    }
    this._list_chanel_connected.push(chanel);
    chanel.addMember(this);
  }

  leaveChanel(chanel) {
    chanel.removeMember(this);
    this._list_chanel_connected.splice(this._list_chanel_connected.indexOf(chanel), 1)
  }

  get list_chanel_connected() {
    return this._list_chanel_connected;
  }

  get modes() {
    return this._modes
  }

  get isOperator() {
    return (this._modes.indexOf('o') > -1) ? true : false;
  }

  get time_connected() {
    return (Date.now() - this._start_connection) / 1000;
  }
}
