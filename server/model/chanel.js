module.exports = class Chanel {
  constructor(nome, topico, modos, senha) {
    this._name = nome;
    this._topic = topico;
    this._current_members = [];
    this._modes =(modos) ? modos : [];
    this._pass = senha;
  }

  addMember(client) {
    this._current_members.push(client);
  }

  removeMember(client) {
    this.current_members = this.current_members.filter(function(el) {
      return el != client;
    });
  }

  set name(value) {
    this.name = value;
  }

  get name() {
    return '#' + this._name;
  }

  get topic() {
    return (this._topic) ? this._topic : '';
  }

  set topic(value) {
    this._topic = value;
  }

  get list_members() {
    return this._current_members;
  }

  get count_members() {
    return this._current_members.length;
  }

  get modes(){
    return this._modes;
  }

  get pass(){
    return this._pass;
  }

  set pass(value) {
    this._pass = value;
  }
}
