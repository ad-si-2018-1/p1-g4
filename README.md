IRC Nodejs
=========================
Descrição
-------------------------

IRC NODEjs é um servidor de mIRC desenvolvido em NODEjs a partir da implementação do protocolo  IRC (Internet Relay Chat). Através do servidor será possível realizar a comunicação entre dois clientes distintos, e realizar operações de conexão (WHOis, definir nick, registrar senha), operações de canais (join, definir tópico, kick usuário), envio de mensagens privadas, consultas ao servidor (motd, info, version, stats), consultas baseadas no usuário (whois, ping), entre outras variadas, uma lista de todas as funções disponíveis pode ser encontrada na documentação.

Instruções / Tutoriais
---------------------

#### Executar servidor 

```
npm run servidor
```

### Executar em modo desenvolvedor (debug - porta 5858) 

```
npm run dev
```

Documentação
---------------------

[Diagrama de Atividades]

[Diagrama de Sequência]

[Plano de teste]

[Mensagens Suportadas]

[Mensagens Não Suportadas]

[Referencias RFC]
* https://nodejs.org/api/net.html

Desenvolvedores
---------------------

### Líder do grupo
* Wellington

### Desenvolvedores
* Jean
* Raquel
* Wellington

### Documentadores
* Jader
* João Victor

### Analista de testes
* Gleydson