Tutorial Instalação, Configuração e Execução IRCNODEjs
=========================

#### Instalação / Execução Servidor.

### Windows
1. Realizar o download do Nodejs (Disponível em: https://nodejs.org/en/download/)
2. Fazer download do repositório (Disponível em: https://gitlab.com/ad-si-2018-1/p1-g4/-/archive/master/p1-g4-master.zip)
3. Instalar Nodejs.
4. Após a instalação do nodejs, abrir o Nodejs e executar o arquivo através do comando:

````
Node \...\p1-g4-master\server\server.js
````

4.1 Opcionalmente, você pode executar pelo prompt de comando, basta ir no Menu INICIAR, digitar cmd, e em seguida o comando:
````
Node \...\p1-g4-master\server\server.js
````

*Obs: Onde refere-se por \...\ dá-se pelo diretório em que o arquivo está localizado.

Após execução, o servidor estará executando, agora devemos seguir para o lado do Cliente. Servidor/Porta: localhost:6667;

### Linux Debian
1. Executar o comando:
```
sudo apt-get install -y nodejs
sudo apt-get install npm
```

2. Fazer download do repositório (Disponível em: https://gitlab.com/ad-si-2018-1/p1-g4/-/archive/master/p1-g4-master.zip)
3. Executar o arquivo do servidor através da linha de comando utilizando o comando
```
Node \...\p1-g4-master\server\server.js
```

Após execução, o servidor estará executando, agora devemos seguir para o lado do Cliente. Servidor/Porta: localhost:6667;


### Instalação / Execução Cliente.

1. Fazer o download do Putty ou HexChat, disponíveis em:
- PuttyTel (https://the.earth.li/~sgtatham/putty/latest/w32/puttytel.exe)
- HexChat (http://xchat.org/files/binary/win32/xchat-2.8.9.exe)

2. Realizar instalação HexChat (Guia disponível em /documentação/Manualhexchat.md)
*Obs: O PuttyTel já é um executável, e irá abrir diretamente do arquivo baixado.
  
3. Configuração PuttyTel
Após a execução, em "Host Name (or Ip Address)" informe o ip do servidor, no caso localhost, e no campo Port, informe a porta para conexão, no caso 6667 e clicar em Open.

HexChat
O tutorial de instalação/configuração do Hexchat encontra-se disponível em /documentação/Manualhexchat.md