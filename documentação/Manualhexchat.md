Tutorial Instalação e Configuração HexChat (Windows)
=========================

Realizar download do HexChat de acordo com seu sistema operacional em: https://hexchat.github.io/downloads.html
---------------------

#### Instalação

```
1. Ao executar o arquivo baixado, será apresentado o Assistente de Instalação do HexChat, clique no botão Avançar.
2. Será apresentado uma tela de escolha de componentes, deixe as opções marcadas como estão e clique em Avançar.
3. Escolha a pasta de destino da instalação e clique no botão Avançar.
4. Selecione um nome de uma pasta no Menu Iniciar (padrão xChat), e clique no botão Instalar.
5. Selecione a opção "Executar xChat" e clique no botão Terminar.
```

### Configuração Inicial e Conexão

```
1. Ao abrir o xChat, em "User Information" preencha seu Nick, a segunda opção do nick, a terceira opção do nick, seu username e nome real.
2. Vá em Networks, e clique em Add e escolha o nome do servidor "Ircnodejs".
3. Selecione o servidor criado, e clique em Edit. 
4. Na nova janela, digite o ipdoservidor/porta e clique em Close.
5. Selecione o servidor Ircnodejs na lista, e clique em Connect.
```